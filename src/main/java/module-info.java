module idatt2001.ntnu.no.cardgames {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.controlsfx.controls;

    opens idatt2001.ntnu.no.cardgames to javafx.fxml;
    exports idatt2001.ntnu.no.cardgames;
}