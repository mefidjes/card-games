package idatt2001.ntnu.no.cardgames;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class Controller {

    private DeckOfCards deckOfCards;
    private Hand hand;
    String baseUrl = "file:src/main/resources/idatt2001/ntnu/no/cardgames/PlayingCards/";

    @FXML
    private ImageView card1;
    @FXML
    private ImageView card2;
    @FXML
    private ImageView card3;
    @FXML
    private ImageView card4;
    @FXML
    private ImageView card5;

    @FXML
    private Button dealHand;

    @FXML
    private Button checkHand;

    @FXML
    private Button resetDeck;

    @FXML
    private Label sumOfFacesLabel;
    @FXML
    private TextField sumOfFacesBox;
    @FXML
    private Label cardsOfHeartLabel;
    @FXML
    private TextField cardsOfHeartBox;
    @FXML
    private Label flushLabel;
    @FXML
    private TextField flushBox;
    @FXML
    private Label queenOfSpadesLabel;
    @FXML
    private TextField queenOfSpadesBox;
    @FXML
    private Label cardsLeftInDeck;

    @FXML
    protected void initialize() {
        deckOfCards = new DeckOfCards();
    }

    @FXML
    private void dealHand() {
        try {
            hand = deckOfCards.dealHand(5);
        } catch (IllegalArgumentException e) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText("Error");
            alert.setContentText("Not enough cards left in the deck to deal");
        }
        Image backOfCard = new Image(baseUrl + "back.png");
        card1.setImage(backOfCard);
        card2.setImage(backOfCard);
        card3.setImage(backOfCard);
        card4.setImage(backOfCard);
        card5.setImage(backOfCard);
        cardsLeftInDeck.setText("Cards left in deck: " + Integer.toString(deckOfCards.getCardAmount()));
    }

    @FXML
    private void checkHand(){
        card1.setImage(new Image(baseUrl + hand.getHand().get(0) + ".png"));
        card2.setImage(new Image(baseUrl + hand.getHand().get(1) + ".png"));
        card3.setImage(new Image(baseUrl + hand.getHand().get(2) + ".png"));
        card4.setImage(new Image(baseUrl + hand.getHand().get(3) + ".png"));
        card5.setImage(new Image(baseUrl + hand.getHand().get(4) + ".png"));

        sumOfFacesBox.setText(Integer.toString(hand.sumOfFaces()));
        if (hand.hasFlush()){
            flushBox.setText("Yes");
        } else {
            flushBox.setText("No");
        }

        if (hand.containsCard('S',12)){
            queenOfSpadesBox.setText("Yes");
        } else {
            queenOfSpadesBox.setText("No");
        }
        cardsOfHeartBox.setText(hand.getAllHearts());
    }

    @FXML
    private void resetDeck(){
        deckOfCards = new DeckOfCards();
        cardsLeftInDeck.setText("Cards left in deck: " + Integer.toString(deckOfCards.getCardAmount()));
    }


}