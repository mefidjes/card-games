package idatt2001.ntnu.no.cardgames;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Hand {
    private final ArrayList<Card> hand;

    /**
     * Contructor that takes in arrayList of cards
     * @param hand
     */
    public Hand(ArrayList<Card> hand) {
        this.hand = new ArrayList<Card>(hand);
    }

    /**
     * Returns the sum of the faces by iterating through the hand and adding the value of the card
     * to a total sum
     * @return Returns the sum of cards in hand
     */
    public int sumOfFaces(){
        int total = 0;
        for (Card card : hand) {
            total += card.getFace();
        }
        return total;
    }

    /**
     * Using stream filters the method loops through the cards and if 5 cards of the same suit are present
     * the methods returns true
     * @return
     */
    public boolean hasFlush(){
        return ((hand.stream().filter(x -> x.getSuit() == 'C').count() >= 5) ||
            (hand.stream().filter(x -> x.getSuit() == 'S').count() >= 5) ||
            (hand.stream().filter(x -> x.getSuit() == 'H').count() >= 5) ||
            (hand.stream().filter(x -> x.getSuit() == 'D').count() >= 5));
    }

    /**
     * Iterates through the hand and checks wheter a card matching the suit and the face is present and if
     * so it returns True.
     * @param suit The suit of the card to be found
     * @param face The face value of the card to be found
     * @return Returns a boolean
     */
    public boolean containsCard(char suit, int face){
        return (hand.stream().filter(x -> x.getSuit() == suit).anyMatch(x -> x.getFace() == face));
    }

    /**
     *
     * @return Returns the number of cards in the hand
     */
    public int getNumberOfCards(){
        return hand.size();
    }

    /**
     *
     * @return Returns the hand
     */
    public ArrayList<Card> getHand() {
        return hand;
    }

    /**
     * Using stream filters it collects all the cards that have the suit H and spereates them by the delimter ", " and
     * returns the cards as a string.
     * @return Returns all the hearts in one string.
     */
    public String getAllHearts(){
        return hand.stream().filter(x -> x.getSuit() == 'H').map(Card::toString).collect(Collectors.joining(", "));
    }

//    public boolean allCardsUnique() {
//        for (Card originalCard : hand) {
//            for (Card cloneCard : hand) {
//                if (originalCard.equals(cloneCard) && hand.indexOf(originalCard) != hand.indexOf(cloneCard)) {
//                    return false;
//                }
//            }
//        }
//        return true;
//    }
}

