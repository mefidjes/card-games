package idatt2001.ntnu.no.cardgames;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class DeckOfCards {
    private ArrayList<Card> deck = new ArrayList<>();

    /**
     * Iterates through the different suits with a for loop for different numbers to create
     * a complete deck with every card.
     */
    public DeckOfCards(){
        char[] suits = {'S', 'H', 'D', 'C'};
        for(char suit : suits) {
            for(int i = 1; i < 14; i++) {
                deck.add(new Card(suit, i));
            }
        }
    }

    /**
     *
     * @return Returns the deck
     */
    public ArrayList<Card> getDeck() {
        return deck;
    }

    /**
     * Gets the total amount of cards left in the deck
     * @return The amount of cards left in the deck
     */
    public int getCardAmount(){return deck.size();}

    /**
     * Using class random, int n amount of cards are drawn and added to the hand methods which collects them and
     * returns them as an arrayList. The cards are additionally removed from the deck.
     * @param n the number of cards to be drawn
     * @return Returns a class hand with the cards in the contructor
     */
    public Hand dealHand(int n){
        Random random = new Random();
        if (n > getCardAmount()) {
            throw new IllegalArgumentException("Not enough cards left in the deck.");
        }
        ArrayList<Card> returnList = new ArrayList<Card>();
        for (int i = 0; i < n; i++) {
            Card tempCard = deck.get(random.nextInt(deck.size()));
            returnList.add(new Card(tempCard.getSuit(), tempCard.getFace()));
            deck.remove(tempCard);
        }
        return new Hand(returnList);
    }
}
