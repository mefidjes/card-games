package idatt2001.ntnu.no.cardgames;

public class Card {
    private final char suit;
    private final int face;

    public Card(char suit, int face) {
        this.suit = suit;
        this.face = face;
    }

    /**
     * @return Returns the suit of a card represented by an int
     */
    public char getSuit() {
        return suit;
    }

    /**
     * @return returns the integer of the face value of a card
     */
    public int getFace() {
        return face;
    }

    /**
     * @return Returns the card as a string with both the Suit and the Face value
     */
    @Override
    public String toString(){
        return suit + Integer.toString(face);
    }
}
