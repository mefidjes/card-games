package idatt2001.ntnu.no.cardgames;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class DeckOfCardsTest {

    @Test
    void newDeck() {
        DeckOfCards testDeck = new DeckOfCards();
        assertEquals(52, testDeck.getDeck().size());
    }


    @Test
    void dealHand() {
        DeckOfCards testDeck = new DeckOfCards();
        assertEquals(testDeck.dealHand(5).getNumberOfCards(), 5);
    }
}