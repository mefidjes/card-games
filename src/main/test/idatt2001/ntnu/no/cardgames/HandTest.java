package idatt2001.ntnu.no.cardgames;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class HandTest {

    @Test
    void hasFlush() {
        ArrayList<Card> cardsInHand = new ArrayList<Card>();
        cardsInHand.add(new Card('H', 3));
        cardsInHand.add(new Card('H', 7));
        cardsInHand.add(new Card('H', 9));
        cardsInHand.add(new Card('H', 1));
        cardsInHand.add(new Card('H', 12));
        Hand testHand = new Hand(cardsInHand);
        assertTrue(testHand.hasFlush());
    }

    @Test
    void noFlush() {
        ArrayList<Card> cardsInHand = new ArrayList<Card>();
        cardsInHand.add(new Card('H', 3));
        cardsInHand.add(new Card('H', 7));
        cardsInHand.add(new Card('H', 9));
        cardsInHand.add(new Card('H', 1));
        cardsInHand.add(new Card('S', 12));
        Hand testHand = new Hand(cardsInHand);
        assertFalse(testHand.hasFlush());
    }

    @Test
    void sumOfFaces() {
        ArrayList<Card> cardsInHand = new ArrayList<Card>();
        cardsInHand.add(new Card('H', 3));
        cardsInHand.add(new Card('H', 7));
        cardsInHand.add(new Card('H', 9));
        cardsInHand.add(new Card('H', 1));
        cardsInHand.add(new Card('H', 12));
        Hand testHand = new Hand(cardsInHand);
        assertEquals(32, testHand.sumOfFaces());
    }

    @Test
    void containsCard() {
        ArrayList<Card> cardsInHand = new ArrayList<Card>();
        cardsInHand.add(new Card('H', 3));
        cardsInHand.add(new Card('H', 7));
        cardsInHand.add(new Card('H', 9));
        cardsInHand.add(new Card('H', 1));
        cardsInHand.add(new Card('H', 12));
        Hand testHand = new Hand(cardsInHand);
        assertTrue(testHand.containsCard('H', 12));
    }

    @Test
    void getAllHearts() {
        ArrayList<Card> cardsInHand = new ArrayList<Card>();
        cardsInHand.add(new Card('H', 3));
        cardsInHand.add(new Card('H', 7));
        cardsInHand.add(new Card('H', 9));
        cardsInHand.add(new Card('H', 1));
        cardsInHand.add(new Card('H', 12));
        Hand testHand = new Hand(cardsInHand);
        assertEquals(testHand.getAllHearts(), "H3, H7, H9, H1, H12");
    }
}